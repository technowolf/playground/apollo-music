/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const { CommandoClient } = require('discord.js-commando')
const activities = require('../data/activity.json')
const winston = require('winston')
const { Player } = require('discord-player')
const playerConfig = require('../utils/playerConfig')
const fs = require('fs')

module.exports = class TroyClient extends CommandoClient {
    constructor(options) {
        super(options)

        // Winston Logger
        this.logger = winston.createLogger({
            transports: [new winston.transports.Console()],
            format: winston.format.combine(
                winston.format.timestamp({ format: 'DD/MMM/YYYY HH:mm:ss' }),
                winston.format.printf(
                    log => `[${log.timestamp}] [${log.level.toUpperCase()}]: ${log.message}`),
            ),
        })
        // Activities
        this.activities = activities
        // Discord Player
        this.player = new Player(this, {
            options:{
                leaveOnEn:false,
                leaveOnStop:false,
                leaveOnEmpty:false,
            },
            ytdlDownloadOptions: {
                filter: 'audioonly',
                leaveOnEmpty: false,
            },
            autoSelfDeaf: true,
        })
        // Discord Player Emotes
        this.emotes = playerConfig.emojis
        // Discord Player filters
        this.filters = playerConfig.filters
        // Register events for discord player
        const player = fs.readdirSync('./playerEvents')
            .filter(file => file.endsWith('.js'))
        for (const file of player) {
            const event = require(`../playerEvents/${file}`)
            this.player.on(file.split('.')[0], event.bind(null, this))
        }
    }
}