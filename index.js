/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Client = require('./base/Client')
const path = require('path')
const chalk = require('chalk')

const config = require('./config.json')

const { formatNumber } = require('./utils/Helper')

const client = new Client({
    commandPrefix: `${config.prefix}`,
    owner: `${config.ownerId}`,
    disableEveryone: true,
})

client.registry.registerDefaultTypes()
    .registerGroups([
        ['music', 'Music commands group '],
    ])
    .registerDefaultGroups()
    .registerDefaultCommands({
        help: false,
        prefix: false,
        unknownCommand: false,
    })
    .registerCommandsIn(path.join(__dirname, 'commands'))


client.on('ready', async () => {

    client.logger.info(
        `[READY] Logged in as ${client.user.tag}! ID: ${client.user.id}`)

    // Push client-related activities
    client.activities.push(
        {
            text: () => `${formatNumber(client.guilds.cache.size)} servers`,
            type: 'WATCHING',
        },
        {
            text: () => `with ${formatNumber(
                client.registry.commands.size)} commands`, type: 'PLAYING',
        },
        {
            text: () => `${formatNumber(client.channels.cache.size)} channels`,
            type: 'WATCHING',
        },
    )

    // Interval to change activity every minute
    client.setInterval(() => {
        const activity = client.activities[Math.floor(
            Math.random() * client.activities.length)]
        const text = typeof activity.text === 'function'
            ? activity.text()
            : activity.text
        client.user.setActivity(text, { type: activity.type })
    }, 60000)

})

client.on('disconnect', event => {
    client.logger.error(`[DISCONNECT] Disconnected with code ${event.code}.`)
    process.exit(0)
})

client.on('error', error => client.logger.error(error.stack))

client.on('warn', warn => client.logger.warn(warn))

client.on('commandError', (command, err) => client.logger.error(
    `[COMMAND:${command.name}]\n${err.stack}`))

client.login(config.token).catch(error => {
    console.log(
        chalk.redBright(`Error while logging in. Message: ${error.message}`))
})
