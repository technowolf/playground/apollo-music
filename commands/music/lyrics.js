/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { Lyrics } = require('@discord-player/extractor')
const { geniusApiKey } = require('../../config.json')
const lyricsClient = Lyrics.init(geniusApiKey)

module.exports = class LyricsCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'lyrics',
            aliases: ['lyrics'],
            guildOnly: true,
            group: 'music',
            memberName: 'lyrics',
            description: 'Sends lyrics for the search query.',
            credit: [
                {
                    name: 'Genius',
                    url: 'https://genius.com',
                    reason: 'Lyrics API',
                    reasonURL: 'https://genius.com/',
                },
            ],
            args: [
                {
                    key: 'searchQuery',
                    prompt: 'Please provide a song name for lyrics',
                    type: 'string',
                },
            ],
        })
    }

    async run(message, { searchQuery }) {
        lyricsClient.search(searchQuery)
            .then(result => {
                if (result === null) {
                    return message.channel.send(
                        `Can't find lyrics for: ${searchQuery}`)
                }
                const trimmedLyrics = result.lyrics.substring(0, 1024)
                const lyricsEmbed = new MessageEmbed()
                    .setColor('BLUE')
                    .setTitle(`${result.title} - ${result.artist.name}`)
                    .setThumbnail(result.thumbnail)
                    .addField('Lyrics', trimmedLyrics)
                    .setFooter(
                        `Powered by ${message.client.user.username} & Genius`,
                        message.client.user.avatarURL())
                    .setTimestamp()
                return message.channel.send(lyricsEmbed)
            },
            )
            .catch(console.error)
    }
}