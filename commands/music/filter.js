/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { filters } = require('../../utils/playerConfig')

module.exports = class FilterCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'filter',
            aliases: ['filters'],
            guildOnly: true,
            group: 'music',
            memberName: 'filter',
            description: 'Add filters to music playback',
            args: [
                {
                    key: 'filter',
                    prompt: 'Please specify a valid filter to enable or disable !',
                    type: 'string',
                    oneOf: filters,
                },
            ],
        })
    }

    async run(message, { filter }) {

        if (!message.member.voice.channel) {
            return message.channel.send(
                `${message.client.emotes.error} - You're not in a voice channel !`)
        }

        if (message.guild.me.voice.channel &&
            message.member.voice.channel.id !==
            message.guild.me.voice.channel.id) {
            return message.channel.send(
                `${message.client.emotes.error} - You are not in the same voice channel !`)
        }

        if (!message.client.player.getQueue(message)) {
            return message.channel.send(
                `${message.client.emotes.error} - No music currently playing !`)
        }

        const filterToUpdate = message.client.filters.find(
            (x) => x.toLowerCase() === filter.toLowerCase())

        if (!filterToUpdate) {
            return message.channel.send(
                `${message.client.emotes.error} - This filter doesn't exist, try for example (8D, vibrato, pulsator...) !`)
        }

        const filtersUpdated = {}

        filtersUpdated[filterToUpdate] = !message.client.player.getQueue(
            message).filters[filterToUpdate]

        await message.client.player.setFilters(message, filtersUpdated)

        if (filtersUpdated[filterToUpdate]) {
            message.channel.send(
                `${message.client.emotes.music} - I'm **adding** the filter to the music, please wait... Note : the longer the music is, the longer this will take.`)
        }
        else {
            message.channel.send(
                `${message.client.emotes.music} - I'm **disabling** the filter on the music, please wait... Note : the longer the music is playing, the longer this will take.`)
        }
    }
}