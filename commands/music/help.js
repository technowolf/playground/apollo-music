/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')
const { MessageEmbed } = require('discord.js')
const { util: { permissions } } = require('discord.js-commando')
const { stripIndents } = require('common-tags')

// noinspection HtmlDeprecatedTag
module.exports = class HelpCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'help',
            aliases: ['commands', 'command-list'],
            group: 'music',
            memberName: 'help',
            description: 'Displays a list of available commands, or detailed information for a specific command.',
            guarded: true,
            args: [
                {
                    key: 'command',
                    prompt: 'Which command would you like to view the help for?',
                    type: 'command',
                    default: '',
                },
            ],
            clientPermissions: ['EMBED_LINKS'],
        })
    }

    async run(message, { command }) {
        if (!command) {
            const embeds = []
            for (let i = 0; i <
            Math.ceil(this.client.registry.groups.size / 10); i++) {
                const nsfw = message.channel.nsfw ||
                    this.client.isOwner(message.author)
                const embed = new MessageEmbed()
                    .setTitle(`Command List (Page ${i + 1})`)
                    .setDescription(stripIndents`
						To run a command, use ${message.anyUsage('<command>')}.
						${nsfw
        ? ''
        : 'Use in an NSFW channel to see NSFW commands.'}
					`)
                    .setColor('DARK_GOLD')
                embeds.push(embed)
            }
            let cmdCount = 0
            let i = 0
            let embedIndex = 0
            for (const group of this.client.registry.groups.values()) {
                i++
                const owner = this.client.isOwner(message.author)
                const commands = group.commands.filter(cmd => {
                    if (owner) return true
                    if (cmd.ownerOnly || cmd.hidden) return false
                    return !(cmd.nsfw && !message.channel.nsfw)
                })
                if (!commands.size) continue
                cmdCount += commands.size
                if (i > (embedIndex * 10) + 10) embedIndex++
                embeds[embedIndex].addField(`❯ ${group.name}`,
                    commands.map(cmd => `\`${cmd.name}\``).join(' '))
            }
            const allShown = cmdCount === this.client.registry.commands.size
            embeds[embeds.length - 1]
                .setFooter(
                    `${this.client.registry.commands.size} Commands${allShown
                        ? ''
                        : ` (${cmdCount} Shown)`}`)
            try {
                const msgs = []
                for (const embed of embeds) {
                    msgs.push(
                        await message.channel.send({ embed }))
                }
                return msgs
            }
            catch {
                return message.reply(
                    'Failed to send message!')
            }
        }
        const userPerms = command.userPermissions
            ? command.userPermissions.map(perm => permissions[perm]).join(', ')
            : 'None'
        const clientPerms = command.clientPermissions
            ? command.clientPermissions.map(perm => permissions[perm])
                .join(', ')
            : 'None'
        return message.say(stripIndents`
			__Command **${command.name}**__${command.guildOnly
    ? ' (Usable only in servers)'
    : ''}
			${command.description}${command.details
    ? `\n${command.details}`
    : ''}
			**Format:** ${command.usage(command.format || '')}
			**Example:** ${command.examples}
			**Aliases:** ${command.aliases.join(', ') || 'None'}
			**Group:** ${command.group.name} (\`${command.groupID}:${command.memberName}\`)
			**NSFW:** ${command.nsfw ? 'Yes' : 'No'}
			**Permissions You Need:** ${userPerms}
			**Permissions I Need:** ${clientPerms}
		`)
    }
}
