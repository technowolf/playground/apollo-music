/*
 * MIT License
 *
 * Copyright (c) 2021 TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

const Command = require('../../base/Command')

module.exports = class QueueCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'queue',
            aliases: ['q'],
            guildOnly: true,
            group: 'music',
            memberName: 'queue',
            description: 'Displays current queue',
        })
    }

    async run(message) {
        if (!message.member.voice.channel) {
            return message.channel.send(
                `${message.client.emotes.error} - You're not in a voice channel !`)
        }

        if (message.guild.me.voice.channel &&
            message.member.voice.channel.id !==
            message.guild.me.voice.channel.id) {
            return message.channel.send(
                `${message.client.emotes.error} - You are not in the same voice channel !`)
        }

        const queue = message.client.player.getQueue(message)

        if (!message.client.player.getQueue(message)) {
            return message.channel.send(
                `${message.client.emotes.error} - No songs currently playing !`)
        }

        message.channel.send(
            `**Server queue - ${message.guild.name} ${message.client.emotes.queue} ${message.client.player.getQueue(
                message).loopMode
                ? '(looped)'
                : ''}**\nCurrent : ${queue.playing.title} | ${queue.playing.author}\n\n` +
            (queue.tracks.map((track, i) => {
                return `**#${i +
                    1}** - ${track.title} | ${track.author} (requested by : ${track.requestedBy.username})`
            }).slice(0, 5).join('\n') +
                `\n\n${queue.tracks.length > 5
                    ? `And **${queue.tracks.length - 5}** other songs...`
                    : `In the playlist **${queue.tracks.length}** song(s)...`}`))
    }
}