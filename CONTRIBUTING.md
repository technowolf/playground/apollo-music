# Contributing

1. File an issue to notify the maintainers about what you're working on.
2. Fork the repo, develop and test your code changes, add docs/comments when necessary.
3. Make sure your commit messages clearly describe the changes.
4. Send a pull request.

## Filing an issue

Use the issue tracker to start the discussion. It is possible that someone else is already working on your idea, your
approach is not quite right, or that the functionality exists already. The ticket you file in the issue tracker will be
used to hash that all out.

## Code Style

* Write in UTF-8 encoding
* Use LF for line separator not CRLF. Check line separator in your favorite text editor or IDE.
* Always use 4 white-spaces indentation (or set one tab as 4 white space)
* Always try to limit line length to 80 characters
* Function names should always be in camelCase
* Class names should always be in PascalCase
* Constants should be in UPPERCASE
* Add Comments where necessary (Please use English).
* Look at the existing style and adhere accordingly

## Forking the repository

Be sure to do relevant tests on commands, classes and methods before making the pull request. You might also want build
the docs yourself, add comments on changes you have done and make sure they're readable.

## Making Pull Request

Once you have made all your changes, tests, make a pull request to move everything back into the main branch of the
repository. Be sure to reference the original issue in the pull request.

<hr>

**Working on your first Pull Request?**
You can learn how from this *free*
series [How to Contribute to an Open Source Project](https://kcd.im/pull-request)

Even though it is GitHub specific tutorial, Things are mostly same in Gitlab.