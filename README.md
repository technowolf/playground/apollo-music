<!--suppress HtmlDeprecatedAttribute -->
<div align="center">
    <h1>
        <br>
        <a href="#">
            <img src="https://gitlab.com/technowolf/playground/apollo-music/-/raw/master/assets/apollo.png"
            alt="Apollo" width="200"></a>
        <br>
        Apollo - Music Bot
        <br>
    </h1>
    <h4 align="center">Apollo is a stripped down version of Troy with only music commands..</h4>
</div>
<div align="center">
    <a href="https://gitlab.com/technowolf/troy/-/blob/master/LICENSE" target="_blank">
        <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg" alt="License: MIT">
    </a>
    <a href="http://makeapullrequest.com" target="_blank">
        <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat" alt="Make a MR">
    </a>
    <a href="https://gitlab.com/technowolf/playground/apollo-music/commits/master">
        <img alt="pipeline status" src="https://gitlab.com/technowolf/troy/badges/master/pipeline.svg" />
    </a>
    <a href="https://www.paypal.me/daksh7011" target="_blank">
        <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat" alt="donate">
    </a>
    <br>
    <br>
</div>

## Overview

Troy is a Discord bot coded in JavaScript with discord.js using commando command framework. Troy is being developed
around fun and moderation.

## Invite

* [Invite](https://discord.com/api/oauth2/authorize?client_id=777157279446466582&permissions=469888071&scope=bot)

## Permissions

Troy needs several permissions to do what it is supposed to do. Every permission Troy needs is explained below.

* **Manage Roles** is needed to mute members, Give them extra roles Troy needs this permission.
* **Kick Members** is self-explanatory.
* **Ban Members** is self-explanatory.
* **Create Invite** is needed to allow owners and other people who build Troy, join your server. Or you want to create a
  invite link whenever you are feeling lazy.
* **Manage Nickname** is needed to manage nicknames across your server.
* **Change Nickname** is needed to change other's nickname.
* **Read Message** is self-explanatory.
* **Send Message** is self-explanatory.
* **Embed Links** is needed because Troy utilize Discord Embeds heavily and denying this permission will break so many
  commands.
* **Attach Files** is needed for upcoming images related commands.
* **Read Message** History is needed to fetch case numbers while moderation command gets fired.
* **Add Reactions** is self-explanatory.

If any new permission is required, You can find an explanation here.

## Installation Guide

**Before you begin**

1. Make sure you have installed Node.js (v15.0.0 or higher) and Git
2. Clone Repository.
3. Change your directory to troy with `cd troy` after cloning.
4. Run `npm install` to install necessary dependencies.
5. Make sure you have ffmpeg installed if you want to use music commands. (ffmpeg-static npm package is already added
   but before raising a bug ticket make sure there is no ffmpeg related error in logs.)
6. Create a file named `config.json` and fill it out as shown in
   `example-config.json`

After you are done with the prerequisites above, run `npm install` to install dependencies on your system. Then start
the bot with `node .` or
`npm start` command.

Note: I will add a thorough guide to set up the bot on Linux, Windows or Mac system in near feature. Meanwhile, if
someone wants to cover this, Open an issue and submit a Merge Request.

## Contribution Guide

Please take a look at the [contributing](CONTRIBUTING.md)  guidelines if you're interested in helping by any means.

Contribution to this project is not only limited to coding help, You can suggest a feature, help with docs, design ideas
or even some typos. You are just an issue away. Don't hesitate to create an issue.

## License

[MIT License](LICENSE) Copyright (c) 2020 TechnoWolf FOSS

Troy is available under terms of MIT license.

## Links

[Issue Tracker](https://gitlab.com/technowolf/troy/issues)
